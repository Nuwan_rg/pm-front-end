import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8082/property/';

class PropertyService {
  getPublicContent() {
    //return axios.get(API_URL + 'all');
    return axios.get('http://localhost:8082/property/all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'create', { headers: authHeader() });
  }
  
/*
  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }*/
}

export default new PropertyService();
